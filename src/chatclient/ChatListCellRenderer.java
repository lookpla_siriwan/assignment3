/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatclient;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.border.EtchedBorder;

/**
 *
 * @author Com01
 */
class ChatListCellRenderer extends JPanel implements ListCellRenderer<ChatDetail> {

    private ImageIcon icon;
    
    public ChatListCellRenderer(){
        this.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
    }
    
    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
    }
    @Override
    public Component getListCellRendererComponent(JList list, 
            ChatDetail value, int index, boolean isSelected, boolean cellHasFocus) {
            
       this.setLayout(new BoxLayout(this,BoxLayout.X_AXIS));
       this.removeAll();
       
       String sender = value.getSenderName();
        System.out.println("SIR");
       int high = 20;
       if(value.getPic()!=null){
           high=70;
       }
       JLabel senderN = new JLabel(sender+" :");
       senderN.setFont(new Font("Courier New",Font.BOLD,16));
       senderN.setForeground(Color.BLUE);
       senderN.setPreferredSize(new Dimension(sender.length()*12+20,high));
       senderN.setMaximumSize(new Dimension(sender.length()*12+20,high));
       senderN.setMaximumSize(new Dimension(sender.length()*12+20,high));
       this.add(senderN);
       
       String message = value.getMessage();

       if(value.getPic()!=null){
            ImageIcon pic = value.getPic();
            JLabel PicLabel = new JLabel(new ImageIcon(pic.getImage().getScaledInstance(70, 70, Image.SCALE_SMOOTH)));
            PicLabel.setPreferredSize(new Dimension(70,high));
            PicLabel.setMaximumSize(new Dimension(70,high));
            PicLabel.setMaximumSize(new Dimension(70,high));
            this.add(PicLabel);
       }else{
           JLabel mess = new JLabel(message);
            mess.setFont(new Font("Courier New",Font.BOLD,14));
//            mess.setForeground(Color.BLUE);
            mess.setPreferredSize(new Dimension(message.length()*9,high));
            mess.setMaximumSize(new Dimension(message.length()*9,high));
            mess.setMaximumSize(new Dimension(message.length()*9,high));
            this.add(mess);
       }
       
      
       
//       if(pic!=null){
////              picPanel pp = new picPanel(pic);
////              this.add(pp);
//            
//       }else{
//            
//       }
//       double outBoundPrice = value.getOutboundFlightPrice();
//       JLabel price1 = new JLabel(String.valueOf(outBoundPrice));
//       price1.setFont(new Font("Courier New",Font.BOLD,20));
//       price1.setForeground(Color.BLUE);
//       price1.setPreferredSize(new Dimension(100,20));
//       price1.setMaximumSize(new Dimension(100,20));
//       price1.setMaximumSize(new Dimension(100,20));
//       this.add(price1);
//       
//       Component rigidArea1 = Box.createRigidArea(new Dimension(20,20));
//       this.add(rigidArea1);
//       
//       String outBoundAirline = value.getOutboundAirline();
//       
//       ImageIcon temp = new ImageIcon(outBoundAirline+".jpg");
//       icon = new ImageIcon(temp.getImage().getScaledInstance(50, 50, Image.SCALE_SMOOTH));
//       JPanel picPanel = new JPanel();
//       JLabel picLabel = new JLabel(icon);
//       picPanel.add(picLabel);
////       picPanel.setBackground(Color.red);
//       picPanel.setPreferredSize(new Dimension(50, 50));
//       picPanel.setMaximumSize(new Dimension(50, 50));
//       picPanel.setMinimumSize(new Dimension(50, 50));
//       this.add(picPanel);
//       
//       Component rigidArea2 = Box.createRigidArea(new Dimension(20, 20));
//       this.add(rigidArea2);
//       
//       JPanel airLinePanel = new JPanel();
//       JLabel airLineLabel = new JLabel(outBoundAirline);
//       airLinePanel.add(airLineLabel);
//       airLinePanel.setPreferredSize(new Dimension(500, 30));
//       airLinePanel.setMaximumSize(new Dimension(500, 30));
//       airLinePanel.setMinimumSize(new Dimension(500, 30));
//       airLineLabel.setFont(new Font("Courier New",Font.BOLD,18));
//       this.add(airLinePanel);
//       
//       Component rigidArea3 = Box.createRigidArea(new Dimension(20, 20));
//       this.add(rigidArea3);
//       
//       JPanel detailPanel = new JPanel();
////       JButton detailButton = new JButton("Detail");
//       JLabel detailLabel = new JLabel("Detail");
//       detailPanel.add(detailLabel);
//       detailPanel.setBackground(Color.BLUE);
//       detailPanel.setPreferredSize(new Dimension(70, 40));
//       detailPanel.setMinimumSize(new Dimension(70, 40));
//       detailPanel.setMaximumSize(new Dimension(70, 40));
       
//       this.add(detailPanel);
       
       return this;
    }
    
    
}

class ChatDetail{
    private String senderName;
    private String message;
    private ImageIcon pic;

    public ChatDetail(String senderName, String message, ImageIcon pic) {
        this.senderName = senderName;
        this.message = message;
        this.pic = pic;
    }

    public String getSenderName() {
        return senderName;
    }

    public String getMessage() {
        return message;
    }

    public ImageIcon getPic() {
        return pic;
    }
    
}

//class picPanel extends JPanel{
//    private ImageIcon pic;
//
//    public picPanel(ImageIcon pic) {
//        this.pic = new ImageIcon(pic.getImage().getScaledInstance(50, 50, Image.SCALE_SMOOTH));
//        setPreferredSize(new Dimension(50, 50));
//        repaint();
//    }
//
//    @Override
//    protected void paintComponent(Graphics g) {
//        super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
//        g.drawImage(pic.getImage(), 0, 0, this);
//        updateUI();
//    }
//    
//}